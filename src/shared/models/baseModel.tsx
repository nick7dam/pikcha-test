export interface IBaseModel {
    id?: Number;
    createdOn?: Date;
    updatedOn?: Date;
}

export default abstract class BaseModel implements IBaseModel {
    protected constructor(
        public id?: Number,
        public createdOn?: Date,
        public updatedOn?: Date
    ){
    }
};
