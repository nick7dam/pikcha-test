import BaseModel from "../baseModel";

export interface ICountry extends BaseModel {
    id?: Number;
    createdOn?: Date;
    updatedOn?: Date;
    name?: string;
    code?: string;
    code3?: string;
    number?: string;
}

export default abstract class Country implements ICountry {
 protected constructor(
    public id?: Number,
    public createdOn?: Date,
    public updatedOn?: Date,
    public name?: string,
    public code?: string,
    public code3?: string,
    public number?: string,
  ){
  }
};
