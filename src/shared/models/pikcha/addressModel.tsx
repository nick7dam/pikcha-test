import BaseModel from "../baseModel";
import {ICountry} from "./countryModel";

export interface IAddress extends BaseModel {
    id?: Number;
    createdOn?: Date;
    updatedOn?: Date;
    address1?: string;
    city?: string;
    country?: ICountry;
    line2?: string;
    postCode?: string;
    residentialAddress?: string;
    state?: string;
    taxCategory?: number;
    taxNumber?: string;
}

export class Address implements IAddress {
  constructor(
    public id?: Number,
    public createdOn?: Date,
    public updatedOn?: Date,
    public address1?: string,
    public city?: string,
    public country?: ICountry,
    public line2?: string,
    public postCode?: string,
    public residentialAddress?: string,
    public state?: string,
    public taxCategory?: number,
    public taxNumber?: string,
  ) {
      this.id = id ? id : undefined;
      this.createdOn = createdOn ? createdOn : new Date();
      this.updatedOn = updatedOn ? updatedOn : new Date();
      this.id = id ? id : undefined;
      this.address1 = address1 ? address1 : '';
      this.city = city ? city : '';
      this.country = country ? country : undefined;
      this.line2 = line2 ? line2 : '';
      this.postCode = postCode ? postCode : '';
      this.residentialAddress = residentialAddress ? residentialAddress : '';
      this.state = state ? state : '';
      this.taxCategory = taxCategory ? taxCategory : undefined;
      this.taxNumber = taxNumber ? taxNumber : '';
  }
};
