import React from 'react';
import i18next from "i18next";

export interface IState {
    numbers: [],
    inputArray: string,
    result: number | any,
}

export interface IProps {
}

export default class SmallestPositiveNumberComponent extends React.Component<IProps, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            numbers: [],
            inputArray: '',
            result: ''
        }
    }

    /*
     * functionName: changeNumbers
     * arguments: e -> Input Event
     * description: splits the numbers from the input and store them in numbers property
     * Author: Nick Dam
     */
    changeNumbers(e: any) {
        this.setState({
            inputArray: e.currentTarget.value,
            numbers: e.currentTarget.value.split(',').map(function (item: string) {
                return parseInt(item, 10);
            })
        });
    }

    /*
    * functionName: Solution
    * arguments: a -> List including positive and negative integers
    * description: Return smallest positive integer that does not occur  in the list
    * Author: Nick Dam
    */
    solution(a: number[]) {
        //sort the array and exclude negative numbers because we need to return a number that is bigger than 0
        let sortedArr = a.filter(item => item >= 1).sort((a, b) => a - b);
        //Number which will be compared with each element of the sorted array
        let toCompare = 1;
        //iterating trough the sorted array in order to find first smallest number that does not appear in the lsit
        sortedArr.forEach(currentNumber => {
            //Check if iterating item is bigger than the number we are comparing if yes return the number else increment toCompare.
            if (toCompare < currentNumber) {
                return this.setState({
                    result: toCompare
                });

            }
            toCompare = currentNumber + 1;
        });
        return this.setState({
            result: toCompare
        });
    }

    render() {
        return (
            <div>
                <label className={'formLabel'}>{i18next.t('Components.enterNumbers')}</label>
                <input className={'formInput'} type={'text'} value={this.state.inputArray} onChange={(e) => this.changeNumbers(e)}/>
                <p className={'holder'}>
                    <button disabled={this.state.numbers.length === 0} className={'formButton'} type={'button'} onClick={(e) => this.solution(this.state.numbers)}>{i18next.t('Buttons.findSmallest')}</button>
                    <span>{i18next.t('Components.smallestNumberIs')}{this.state.result}</span>
                </p>
            </div>
        )
    }
}
