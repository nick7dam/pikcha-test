import React from 'react';
import i18next from "i18next";
import {IAddress} from 'shared/models/pikcha/addressModel';
import AddressService from 'shared/services/pikcha/addressService';
import Select from "react-select";
import {ICountry} from "../../../shared/models/pikcha/countryModel";
import {CountriesList} from "../../../shared/common/countriesList";
import TaxNumberComponent from '../taxNumber/taxNumberComponent';
export interface IState {
    currentAddress: IAddress,
    selectedCountry: ICountry,
    allCountries: any[],
    haveTaxNumber: boolean
}

export interface IProps {
    address: IAddress
}

export default class AddressComponent extends React.Component<IProps, IState> {
    public addressService: AddressService;
    public changeTax: any;

    constructor(props: any) {
        super(props);
        this.state = {
            currentAddress: props.address,
            selectedCountry: {},
            allCountries: [],
            haveTaxNumber: false
        };
        this.addressService = new AddressService();
        this.changeTax = this.changeTaxNumber.bind(this)
    }

    componentDidMount() {
        let countries: any[] = [];
        CountriesList.forEach(country => {
            countries.push({
                label: country.name,
                value: country
            });
        });
        this.setState({
            allCountries: countries
        });
    }
    /*
    * functionName: changeCountry
    * arguments: country -> ICountry
    * description: Set set of selectedCountry with the chosen value
    * Author: Nick Dam
    */
    changeCountry(country: any) {
        let addr = this.state.currentAddress;
        addr.country = country.value;
        this.setState({
            selectedCountry: country,
            currentAddress: addr
        });
    }
    /*
    * functionName: setAddress
    * arguments: field -> Which field should be updated, e -> Input Event
    * description: Set corresponding field of the address object and set state of currentAddress
    * Author: Nick Dam
    */
    setAddress(field: string, e: any) {
        let addr: any = this.state.currentAddress;
        addr[field] = e.currentTarget.value;
        this.setState({
            currentAddress: addr
        });
    }
    /*
    * functionName: hasTaxNumber
    * arguments: e -> Input Event
    * description: set state of haveTaxNumber property which affects the displaying of the additional component where user can input tax number
    * Author: Nick Dam
    */
    hasTaxNumber(e: any) {
        if (!e.target.checked) this.changeTaxNumber('');
        this.setState({
            haveTaxNumber: e.target.checked
        });
    }
    /*
    * functionName: changeTaxNumber
    * arguments: taxNumber -> Changed tax number
    * description: this func is called from the child component 'TaxNumberComponent' when taxNumber input is changed
    * Author: Nick Dam
    */
    changeTaxNumber(taxNumber: string) {
        let addr = this.state.currentAddress;
        addr.taxNumber = taxNumber;
        this.setState({
            currentAddress: addr
        });
    }
    /*
    * functionName: submitForm
    * arguments: /
    * description: This function should be called when the form is submitted
    * Author: Nick Dam
    */
    submitForm() {
    }

    render() {
        return (
            <div>
                <div className={'row'}>
                    <div className={'col-4'}>
                        <pre>
                            <code>
                                {i18next.t('Components.address')}{JSON.stringify(this.state.currentAddress, null, 4)}
                            </code>
                        </pre>
                    </div>
                    <div className={'col-8'}>
                        <form onSubmit={this.submitForm}>
                            <h1 className={'formTitle'}>{i18next.t('Components.form')}</h1>
                            <div className={'formGroup'}>
                                <label className={'formLabel'}>{i18next.t('Components.residentialAddress')}</label>
                                <input className={'formInput'} onChange={(e) => {
                                    this.setAddress('address1', e)
                                }} type="text" value={this.state.currentAddress.address1}/>
                            </div>
                            <div className={'formGroup'}>
                                <label className={'formLabel'}>{i18next.t('Components.line2')}</label>
                                <input className={'formInput'} onChange={(e) => {
                                    this.setAddress('line2', e)
                                }} type="text" value={this.state.currentAddress.line2}/>
                            </div>
                            <div className={'formGroup'}>
                                <label className={'formLabel'}>{i18next.t('Components.cityOrTown')}</label>
                                <input className={'formInput'} onChange={(e) => {
                                    this.setAddress('city', e)
                                }} type="text" value={this.state.currentAddress.city}/>
                            </div>
                            <div className={'formGroup'}>
                                <label className={'formLabel'}>{i18next.t('Components.postCode')}</label>
                                <input className={'formInput'} onChange={(e) => {
                                    this.setAddress('postCode', e)
                                }} type="text" value={this.state.currentAddress.postCode}/>
                            </div>
                            <div className={'formGroup'}>
                                <label className={'formLabel'}>{i18next.t('Components.state')}</label>
                                <input className={'formInput'} onChange={(e) => {
                                    this.setAddress('state', e)
                                }} type="text" value={this.state.currentAddress.state}/>
                            </div>
                            <div className={'formGroup'}>
                                <label className={'formLabel'}>{i18next.t('Components.country')}</label>
                                <Select value={this.state.selectedCountry} onChange={this.changeCountry.bind(this)} options={this.state.allCountries}/>
                            </div>
                            <div className={'formGroup'}>
                                <input type={'checkbox'} checked={this.state.haveTaxNumber} onChange={(e) => {
                                    this.hasTaxNumber(e)
                                }}/>
                                <label className={'formLabel'}>{i18next.t('Components.taxNumberQuestion')}</label>
                            </div>
                            {
                                this.state.haveTaxNumber ?
                                    <div className={'formGroup'}>
                                        <TaxNumberComponent taxNumber={this.state.currentAddress.taxNumber} onValChange={this.changeTax}></TaxNumberComponent>
                                    </div> : ''
                            }
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
