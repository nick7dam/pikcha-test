import React from 'react';
import i18next from "i18next";

export interface IState {

}

export interface IProps {
    taxNumber: any,
    onValChange: any
}

export default class TaxNumberComponent extends React.Component<IProps, IState> {
    /*
    * functionName: changeTaxNumber
    * arguments: e -> Input Event
    * description: This function is calling a parent function send as a property in order to change the tax number
    * Author: Nick Dam
    */
    changeTaxNumber(e:any) {
        this.props.onValChange(e.currentTarget.value);
    }


    render() {
        return (
            <div>
                <p className={'boldText'}>{i18next.t('Components.myTaxNumber')}</p>
                <hr/>
                <div className={'formGroup'}>
                    <input type={'text'} className={'formInput'} value={this.props.taxNumber} onChange={(e) => {this.changeTaxNumber(e)}}/>
                </div>
            </div>
        )
    }
}
